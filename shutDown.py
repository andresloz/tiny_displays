#! /usr/bin/env python
#-*- coding: utf-8 -*-

import os, time
import RPi.GPIO as GPIO

# add to rc.local
# python /home/pi/shutDown.py &
# python /home/pi/[name of script].py &"

if __name__ == "__main__":
	GPIO.setmode(GPIO.BCM)

	# button is pin -> button -> ground
	shutdownButton = 0
	playButton = 0
	playScript = "python /home/pi/[name of script].py &"
	
	if shutdownButton:GPIO.setup(shutdownButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # shutdown system
	if playButton:GPIO.setup(playButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # play button
	
	while 1:# Now wait!
		if shutdownButton:
			shutDown_state = GPIO.input(shutdownButton)
			if shutDown_state == 0:
				os.system("sudo shutdown -h now")

		if playButton:
			play_state = GPIO.input(playButton)
			if play_state == 0:
				time.sleep(1)
				os.system(playScript)

