#! /usr/bin/env python
#-*- coding: utf-8 -*-

import sys, os
from PIL import Image

NAME = "Oled SSD130x 128x64 screen convert image"
VERSION = "0.0.3"
DATE = "2018 April 22"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://andre-lozano.org"
DESCRIPTION = "convert image to oled display 128x64 SSD1306/SSD1308, usage: python image2bin.py filename"
"""
Image to Oled screen: example with 128 x 64 pixels screen
	|		page 		0		|		|		page		1		|	...	16	pages
	0	1	2	3	4	5	6	7		8	9	10	11	12	13	14	15	...	128 bytes by line 
0	byt	byt	byt	byt	byt	byt	byt	byt		byt	byt	byt	byt	byt	byt	byt	byt	...

	each page => 8 bytes
	each byte => 8 pixels
bit	0	x	x	x	x	x	x	x	LSB bits order, up to down
bit	1	x	x	x	x	x	x	x	each bit equal one pixel
bit	2	x	x	x	x	x	x	x
bit	3	x	x	x	x	x	x	x
bit	4	x	x	x	x	x	x	x
bit	5	x	x	x	x	x	x	x
bit	6	x	x	x	x	x	x	x
bit	7	x	x	x	x	x	x	x

1	byt	byt	byt	byt	byt	byt	byt	byt		byt	byt	byt	byt	byt	byt	byt	byt	...
2	byt	byt	byt	byt	byt	byt	byt	byt		byt	byt	byt	byt	byt	byt	byt	byt	...
3	byt	byt	byt	byt	byt	byt	byt	byt		byt	byt	byt	byt	byt	byt	byt	byt	...
4	byt	byt	byt	byt	byt	byt	byt	byt		byt	byt	byt	byt	byt	byt	byt	byt	...
5	byt	byt	byt	byt	byt	byt	byt	byt		byt	byt	byt	byt	byt	byt	byt	byt	...
7	byt	byt	byt	byt	byt	byt	byt	byt		byt	byt	byt	byt	byt	byt	byt	byt	...	8	rows
																		...	1024 bytes (8192 pixels) = 128x64 screen
"""
def getBytes(img=None):
	# You can convert a batch of files using this function to a list of files
	width, height = (128,64)
	img = img.resize((width, height))
	img = img.convert("1") # array of 0 or 255 values
	pixels = list(img.getdata())
	
	byteArray = []
	index = 0
	for pageRow in xrange(height/8):
		for x in xrange(width):# Iterate through all x axis columns.
			bits = 0 # Set the bits for the column of pixels at the current position.
			for bit in [7, 6, 5, 4, 3, 2, 1, 0]: # reverse order
				bits = bits << 1 # LSB
				bits |= 1 if pixels[x + ((pageRow*8+bit)*width)] > 0 else 0 # add bit

			byteArray.append(bits)

	return byteArray

if __name__ == "__main__":
	if len(sys.argv) > 1:
		file = sys.argv[1]
		dir = os.path.dirname(file)
		base, ext = os.path.splitext(os.path.basename(file))
	else:
		file = "bacall.png"
		dir = os.path.dirname(os.path.realpath(__file__))
		base = "bacall"
	
	print dir
	# convert image to oled screen
	if base:
		image = Image.open(file)
		bytes = getBytes(image)
		file = os.path.join(dir, base+".bin")
		with open(file,"wb") as f:
			f.write(bytearray(bytes))
