# Oled I2C SSD1306

## Python oled class

Here 3 classes using Adafruit librairies  
All this code was tested on a raspberry pi A, B, Zero  
  
first install Adafruit  
sudo pip install Adafruit-GPIO  
sudo pip install Adafruit-SSD1306  

oled128x32c.py  oled 128x32 I2C  
oled128x64c.py  oled 128x64 I2C  
oled128x64s.py  oled 128x32 SPI  

**Andres Lozano Gallego a.k.a Loz, 2021.**  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License http://www.artlibre.org
