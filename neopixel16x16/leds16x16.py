#! /usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import print_function
from os import listdir, path, system
import sys, time, tarfile, RPi.GPIO as GPIO
from leds import Leds

try:
	import pygame
	audioK = 1
except:
	audioK = 0

NAME = "Show Pixels"
VERSION = "0.0.1"
DATE = "2021 August 15"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://hyperficiel.com"
DESCRIPTION = "show text, read data from archive of pictures files and send them to display 16x16 led"

"""
example with raspberry pi
in order to use this script you have to install spi and unicornhat
if your rapsberry pi have sound output you must to install pygame if you want sound

this script is dedicated for videos done with "Movie Pattern" module from "PMT (pattern maker tools)" by Loz from http://andre-lozano.org
put directories which contains tar archive, sound and frameset in "videos" directory
put videos and unihat directories in the same directory as this file
add "python /home/pi/leds16x16.py &" in /etc/rc.local if you want autostart
"""

def getDirectories(dir=".", exclude=[]):
	directories = []
	for file in sorted(listdir(dir)):
		if path.isdir(path.join(dir, file)):
			if not file in exclude:
				directories.append(path.join(dir, file))

	return directories

def loadMusic(file=None, vol=0.1):
	if audioK:
		try:
			pygame.mixer.music.load(file)
			pygame.mixer.music.set_volume(vol)
		except:
			pass

def playMusic(file=None):
	if audioK:
		try:
			pygame.mixer.music.play()
		except:
			pass

def stopMusic():
	if audioK:
		try:
			pygame.mixer.music.stop()
		except:
			pass

def processFiles(frames=[], screen=None, params={}):
	for k,v in params.iteritems():
		exec(k+'=v')
			
	if path.isfile(videoArchive) and path.isfile(audioArchive) and path.isfile(frameRateFile):
			# prepare data
			if len(frames) == 0:# check if frames are yet loaded
				tar = tarfile.open(videoArchive)
				prevPix = 0
				for i, member in enumerate(tar.getmembers()):
					f = tar.extractfile(member)
					data = eval(f.read())
					frames.append(data)

					shape = [
						# 0     1     2     3     4     5     6     7     8
						[7,6],[6,6],[6,7],[6,8],[7,8],[8,8],[8,7],[8,6],[7,6]
					]
					if i % 10 == 0:
						if prevPix < 7:
							prevPix += 1
						else:
							prevPix = 0

						nextPix = prevPix + 1

						off = [shape[prevPix][0], shape[prevPix][1], 0, 0, 0]
						on = [shape[nextPix][0], shape[nextPix][1], 255, 0, 0]

						screen.setPixels([off,on])
						screen.show(duration=10)

					if nextDirButton:
						input_state = GPIO.input(nextDirButton)
						if input_state == 0: # get next directory
							screen.clear()
							time.sleep(1)
							return []

					if exitButton:
						exit_state = GPIO.input(exitButton)
						if exit_state == 0:
							screen.clear()
							screen.off()
							tar.close()
							exit()

				tar.close()

			with open(frameRateFile) as f: # 0.04 = 1 sec / 25 frames
				frameDuration = 1 / float(f.read())

			# start process
			elapsedTime = time.time() # start elapsed time
			playMusic(audioArchive)

			for i,frame in enumerate(frames):
				if i == 0:
					screen.scrollScreen(data=frame, bg=(0,0,0), duration=50)
				else:
					screen.setBuffer(frame)

				if rotate:screen.rotate(angle=rotate)

				screen.show()
				elapsedTime += frameDuration

				timeLag = elapsedTime - time.time()
				if timeLag > 0.01: # control time frame in millisecond
					time.sleep(timeLag)

				if nextDirButton:
					input_state = GPIO.input(nextDirButton)
					if input_state == 0 : # get next directory
						screen.clear()
						stopMusic()
						time.sleep(1)
						return []

				if exitButton:
					exit_state = GPIO.input(exitButton)
					if exit_state == 0:
						screen.clear()
						screen.off()
						stopMusic()
						exit()

			stopMusic()
			# end process 
	return frames

if __name__ == "__main__":
	GPIO.setmode(GPIO.BCM)

	# button connected like: pin -> button -> ground
	nextDirButton = 26
	exitButton = 19

	if nextDirButton:GPIO.setup(nextDirButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # next directory button
	if exitButton:GPIO.setup(exitButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # exit from show button

	screen = Leds(host="localhost:7890", params={"size":(16,16), "matrix":8, "posterize":2})

	screen.clear()

	#  movies parameters
	d = 0 # dirs in use (videos)
	frames = [] # video frames

	if audioK:
		try:
			pygame.init()
		except:
			audioK = 0

	videoDirectories = getDirectories(dir=path.join(path.dirname(path.realpath(__file__)), "videos"))

	frames = []
	try:
		while True:
			if len(videoDirectories) == 0:exit()
			elif len(videoDirectories) == 1: # one directory then loop contigous
				directory = videoDirectories[0]
				screen.scrollText(text=directory.split('/')[-1], xy=(0,4), color="blue")

				params = {
				"videoArchive" : path.join(directory, "archive.tar"),
				"audioArchive" : path.join(directory, "audio.mp3"),
				"frameRateFile" : path.join(directory, "framerate.txt"),
				"rotate":0
				}

				screen.clear()
				frames = processFiles(frames=frames, screen=screen, params=params)

			elif len(videoDirectories) > 1:
				# loop
				if d == len(videoDirectories): d = 0

				directory = videoDirectories[d]
				screen.scrollText(text=directory.split('/')[-1], xy=(0,4), color="blue")

				params = {
				"videoArchive" : path.join(directory, "archive.tar"),
				"audioArchive" : path.join(directory, "audio.mp3"),
				"frameRateFile" : path.join(directory, "framerate.txt"),
				"rotate":0
				}

				screen.clear()
				# ~ reset frames for each directory
				processFiles(frames=[], screen=screen, params=params)

				d += 1 # go next directory
	except KeyboardInterrupt:
		screen.clear()
		screen.off()
		stopMusic()
		print("stop while")
