# Diaporama with neopixel leds

This project use NeoMatrix 8x8 leds device  for a 16x16 screen
using fadecandy to drive them  
All is drived by python code on Raspberry pi Zero  
[NeoMatrix 8x8 - 64 RGB LED Pixel Matrix](https://www.adafruit.com/product/1487)  
[FadeCandy - Dithering USB-Controlled Driver for RGB NeoPixels](https://www.adafruit.com/product/1689)  

**Andres Lozano Gallego a.k.a Loz, 2018.**  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License http://www.artlibre.org

