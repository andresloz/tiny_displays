#! /usr/bin/env python
#-*- coding: utf-8 -*-
from __future__ import print_function, division
import time
import opc
from ascii_8x8_pixel import AsciiPixels
from colors import Color

class Leds:
	def __init__(self, host=None, params={"size":(16,16), "matrix":8, "posterize":3}):
		self.leds = opc.Client(host)
		
		for k,v in params.iteritems():
			setattr(self,k,v)

		self.numLeds = self.size[0]*self.size[1]
		self.buffer = [(0,0,0)]*self.numLeds
		self.offScreen = [(0,0,0)]*self.numLeds

	def posterizeList(self, data=[(0,0,0)]):
			# to prevent excess led power consumption
			# posterize byte using shift operator ex: posterize val = 256 >> 3
			dataPosterized = [tuple([val >> self.posterize for val in color]) for color in data]
			return dataPosterized

	def getMatrixNxN(self):
		N = self.matrix
		width, height = self.size
		wMx = (width // N)
		hMx = (height // N)
		matrix = [(0,0,0)]*self.numLeds
		idx = 0
		for l in xrange(hMx):
			for k in xrange(wMx):
				for j in xrange(l*N,(l*N)+N):
					for i in xrange(k*N,(k*N)+N):
						matrix[idx] = self.buffer[(j*width)+i]
						idx += 1
		return matrix
		
	def rotate(self, angle=90):
		def get90(dat=None, size=None):
			w,h = size
			imageData = []
			for x in xrange(w):
				tmp = []
				for y in reversed(xrange(h)):
					tmp.append(dat[x+(w*y)])
				imageData.extend(tmp)

			return (h,w),imageData
			
		if angle >= 90:
			newSize, imageData = get90(dat=self.buffer[:], size=self.size) # 90

		if angle >= 180:
			newSize, imageData = get90(dat=imageData, size=newSize) # 180
			
		if angle == 270:
			newSize, imageData = get90(dat=imageData, size=newSize) # 270

		self.buffer = imageData

	def setPixels(self, data=[(0,0,0,0,0)]):
		# ~ list of tuples like [(x,y,r,g,b)...]
		for dat in data:
			x,y,r,g,b = dat
			idx = (self.size[0]*y)+x
			if idx < self.numLeds:
				self.buffer[idx] = (r,g,b)

	def setBuffer(self, data=[(0,0,0)]):
		self.buffer = data[0:self.numLeds] # prevent excess of data

	def clearBuffer(self):
		self.buffer = [(0,0,0)]*self.numLeds

	def show(self, duration=0):
		if self.matrix:
			data = self.getMatrixNxN()
		else:
			data = self.buffer[:]
		
		if self.posterize:
			data = self.posterizeList(data=data)
			
		self.leds.put_pixels(data)
		if duration:time.sleep(duration/1000)

	def clear(self):
		self.buffer = [(0,0,0)]*self.numLeds
		self.leds.put_pixels(self.offScreen)
		time.sleep(0.01)

	def scrollText(self, text=None, xy=(0,0), color=(127,127,127), duration=20):
		if text:
			spc = " " * (self.size[0] // 8)
			text = spc+text # put space at start
			x,y = xy # start position for text
			charLength = 8
			for charOrder,char in enumerate(text):
				for step in range(charLength): # 8 for char pixels length
					self.clearBuffer()
					self.setPixels(self.getChar(char=char,xy=(x-step,y),color=color))
					# ~ init next char 
					for nextCharOrder in range(1,(self.size[0]//charLength)+1): # 1 +1 next col decalage
						try:
							self.setPixels(self.getChar(char=text[charOrder+nextCharOrder],xy=((x-step)+(charLength*nextCharOrder),y),color=color))
						except:
							pass

					self.show()
					time.sleep(duration/1000)

			self.clear()
			return 1 
		else:
			return 0

	def getChar(self, char=None, xy=None, color=None):
		data = []
		charArray = AsciiPixels().getCharData(char)
		r,g,b = Color().getRGB(color)
		for y in range(8): # each line of char
			for x in range(8): # each pixel
				# ~ check if char don't go away out from screen
				if 0 <= xy[0]+x < self.size[0] and 0 <= xy[1]+y < self.size[1] and charArray[y][x] == 1: # == 1 if pixel lighted
					data.append((xy[0]+x,xy[1]+y ,r,g,b))

		return data
		
	def scrollScreen(self, data=None, right=1, bg=None, duration=20):
		if data: self.buffer = data
		width,height = self.size
		for step in range(width):
			scroll = []
			for widthStep in range(width):
				row = self.buffer[(widthStep*width):(widthStep*width)+width]
				if right:
					last = [row.pop()]
					if bg:
						scroll.extend([bg]+row)
					else:
						scroll.extend(last+row)
				else:
					first = [row[0]]
					if bg:
						scroll.extend(row[1:]+[bg])
					else:
						scroll.extend(row[1:]+first)

			self.buffer = scroll
			self.show()
			
			time.sleep(duration/1000)

	def off(self):
		self.leds.disconnect()
		return

