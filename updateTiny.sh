cd tiny_displays
git pull
cd ~

# 128x128 
#rm -fr oledssd1327
#cp -r tiny_displays/oledssd1327-128x128/oledssd1327 .

# 96x96
#rm -fr oledssd1327
#cp -r tiny_displays/oledssd1327-96x96/oledssd1327 .

#rm -fr oledssd1306
#cp -r tiny_displays/oledssd1306 .

#rm -fr oledssd1308
#cp -r tiny_displays/oledssd1308 .

# With Adafruit library
#rm -f oled128x64c.py
#cp tiny_displays/oledssd1306-Ada/oled128x64c.py .

#rm -f oled128x64s.py
#cp tiny_displays/oledssd1306-Ada/oled128x64c.py .

#rm -f oled128x32c.py
#cp tiny_displays/oledssd1306-Ada/oled128x32c.py .

#binclock
#rm -f lcdrgb
#cp tiny_displays/lcdrgb .

#serial
#rm -f lcdserial
#cp tiny_displays/lcdserial .

#unihat
#rm -f unihat
#cp tiny_displays/unihat .
