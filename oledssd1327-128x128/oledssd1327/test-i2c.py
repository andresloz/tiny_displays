#! /usr/bin/env python
#-*- coding: utf-8 -*-
import time
from oledSSD1327 import OledSSD1327

if __name__ == "__main__":
	oled = OledSSD1327(I2C=1)
	
	oled.init_OLED()
	
	heartPixels = [ 									# page pixels description
			0, 0, 0, 0, 0, 0, 0, 0,
			0, 1, 1, 0, 1, 1, 0, 0,
			1, 1, 1, 1, 1, 1, 1, 0,
			1, 1, 1, 1, 1, 1, 1, 0,
			0, 1, 1, 1, 1, 1, 0, 0,
			0, 0, 1, 1, 1, 0, 0, 0,
			0, 0, 0, 1, 0, 0, 0, 0,
			0, 0, 0, 0, 0, 0, 0, 0
		]
	
	try:
		while True:
			oled.clearDisplay()
			time.sleep(1)
			row = 0
			# intro text
			oled.showTextAt(row=row+7, align="center",  string="The Look")
			
			for i in xrange(16): # fill row
				oled.showPageAt(row=row, col=i, data=heartPixels)

			for i in xrange(1,15):
				oled.showPageAt(row=row+i, col=0,  data=heartPixels)
				oled.showPageAt(row=row+i, col=15, data=heartPixels)
			
			for i in xrange(16): # fill row
				oled.showPageAt(row=row+15, col=i, data=heartPixels)

			time.sleep(2)
			
			oled.clearDisplay()
			
			try:
				with open("bacall.bin", "rb") as f:
					imageData = map(ord, f.read())
			except:
				print "can't open file",file
			
			oled.fillScreen(imageData)
			time.sleep(3)

			# add scroll text on top
			oled.setScroll(speed=2, endRow=0, endCol=15)
			oled.showTextAt(string="Glamour Star")
			time.sleep(0.5)
			oled.activeScroll()
			time.sleep(5)
			oled.desactiveScroll()

			oled.fillScreen(imageData)

			oled.setScroll(to="left", speed=2, startRow=15, endRow=15, endCol=15)	
			oled.showTextAt(string="L.Bacall", row=15, align="right")
			time.sleep(2)
			oled.activeScroll()
			time.sleep(3)
			oled.desactiveScroll()

			oled.inverseDisplay()

			oled.fillScreen(imageData)

			oled.setScroll(speed=25, endRow=0, endCol=15)
			oled.showTextAt(string="The Look")
			time.sleep(2)
			oled.activeScroll()
			time.sleep(5)
			oled.desactiveScroll()

			oled.fillScreen(imageData)

			oled.setScroll(to="left", speed=25, startRow=15 , endRow=15, endCol=15)	
			oled.showTextAt(string="of Hollywood", row=15, align="right")
			time.sleep(2)
			oled.activeScroll()
			time.sleep(5)
			oled.desactiveScroll()

			oled.normalDisplay()

			oled.fillScreen(imageData)

			for i in [2,3,4,5,25,64]:
				oled.setScroll(speed=i)
				oled.activeScroll()
				time.sleep(2)
				oled.desactiveScroll()
	except KeyboardInterrupt:
		oled.desactiveScroll()
		oled.normalDisplay()
		oled.clearDisplay()
		print " exit"
