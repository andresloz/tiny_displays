#! /usr/bin/env python
#-*- coding: utf-8 -*-

import sys, os
from PIL import Image
"""
Image to Oled screen: example with 128 x 128 pixels screen
each byte => 2 gray pixels
		pixel 0					pixel 1
0000-1111 	16 levels	|0000-1111 	16 levels
left pixel				|right pixel
0xf0 					|0x0f

0	byte	byte	byte	byte	byte	byte	byte	byte	64 bytes
1	byte	byte	byte	byte	byte	byte	byte	byte	...
2	byte	byte	byte	byte	byte	byte	byte	byte	...
3	byte	byte	byte	byte	byte	byte	byte	byte	...
4	byte	byte	byte	byte	byte	byte	byte	byte	...
5	byte	byte	byte	byte	byte	byte	byte	byte	...
...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	...	128	rows
																...	8192 bytes (16384 pixels) = 128x128 screen
"""
def pixelsToOledGray(pixels=None, size=None):
		# ~ oledssd 1327 structure
		data = []
		for i in xrange(0,len(pixels),2):
			# ~ take two half-bytes side by side to make one => two pixels
			"""
			1111	1111
			16b		16b
			-> 256 bit
			"""
			# convert gray byte to 0-15 values by >>4
			byte1 = pixels[i]   >> 4
			byte2 = pixels[i+1] >> 4

			byte = 0x00
			byte = byte | ((byte1 << 4) & 0xF0)
			byte = byte | (byte2 & 0x0F)

			data.append(byte)

		return data

if __name__ == "__main__":
	if len(sys.argv) > 1:
		file = sys.argv[1]
		dir = os.path.dirname(file)
		base, ext = os.path.splitext(os.path.basename(file))
	else:
		file = "bacall.png"
		dir = os.path.dirname(os.path.realpath(__file__))
		base = "bacall"
	
	print dir
	# convert image to oled screen
	if base:
		image = Image.open(file)
		image = image.resize((128,128))
		image = image.convert("L")
		data = list(image.getdata())
		bytes = pixelsToOledGray(data)
		file = os.path.join(dir, base+".bin")
		with open(file,"wb") as f:
			f.write(bytearray(bytes))
