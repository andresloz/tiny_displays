# Oled SPI/i2c SSD1327

## Python oled class

The class **oledSSD137.py** allow you to show texte and graphics on a  
[Waveshare 1.5inch OLED Display Module 128x128 16 Gray Scale SPI/I2C](https://www.waveshare.com/wiki/1.5inch_OLED_Module) with a Raspberry Pi  
There is an example of code within the class itself  
There is also a script **convert-image-to-oled.py** that convert a picture to an oled binary file  
All this code was tested on a raspberry pi B+

convert-image-to-oled.py require python python PIL ibrary  

For better animation (25 frames/sec) set i2c baudrate to 300 kHz (tested on raspi B+)  

```bash
sudo nano /boot/config.txt
```

160000 <=> 100 kHz on raspberry pi B, B+ and Zero  
480000 = 300 kHz  
```bash
remove comment to : dtparam=i2c_arm=on  
add line: dtparam=i2c1_baudrate=480000  
```
**Andres Lozano Gallego a.k.a Loz, 2018.**  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License http://www.artlibre.org
