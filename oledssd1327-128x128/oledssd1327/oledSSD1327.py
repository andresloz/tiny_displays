 # -*- coding:UTF-8 -*-

import spidev #SPI
import smbus  #I2C	

import RPi.GPIO as GPIO
import time, textwrap

from ascii_8x8_pixel import AsciiPixels

NAME = "Oled SSD1327 screen"
VERSION = "0.0.1"
DATE = "2021 October 23"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://andre-lozano.org"
DESCRIPTION = "oled display 128x128 SSD1327"

class OledSSD1327:
	def __init__(self, SPI=1, I2C=0, i2cAddr=0x3d, size=(128,128), scanMethod=1):
		GPIO.setmode(GPIO.BCM)
		GPIO.setwarnings(False)	
		self.OLED_RST_PIN = 25
		self.OLED_DC_PIN  = 24
		self.OLED_CS_PIN  = 8
		GPIO.setup(self.OLED_RST_PIN, GPIO.OUT)
		GPIO.setup(self.OLED_DC_PIN, GPIO.OUT)
		GPIO.setup(self.OLED_CS_PIN, GPIO.OUT)

		if SPI == 1 and I2C == 1:SPI = 0

		self.USE_SPI_4W = SPI
		self.USE_I2C = I2C
		
		if self.USE_SPI_4W == 1:#Initialize SPI	
			self.SPI = spidev.SpiDev(0, 0)
			self.SPI.max_speed_hz = 20000000
			self.SPI.mode = 0b00
		elif self.USE_I2C == 1:#Initialize I2C
			GPIO.output(self.OLED_DC_PIN, GPIO.HIGH)
			self.bus = smbus.SMBus(1)
			self.OLED_Command_Mode = 0X00
			self.OLED_Data_Mode = 0X40
			self.OLED_Address = i2cAddr

		self.oled_max_width, self.oled_max_height = size  #OLED width,height maximum memory 

		self.oled_x	= 0
		self.oled_y	= 0
		self.oled_width  = (self.oled_max_width - 2 * self.oled_x)	#OLED width
		self.oled_height =  self.oled_max_height               		#OLED height
		
		self.OLED_Dis_Column = self.oled_width
		self.OLED_Dis_Page = self.oled_height
		self.oled_x_Adjust = self.oled_x
		self.oled_y_Adjust = self.oled_y
		
		# pages grid
		self.oled_rows = self.oled_height / 8 # 8 pixels by row
		self.oled_cols = self.oled_width / 8 # 8 pixels by width

		#buffer
		self.buffer = [0]*(int(self.oled_width / 2) * self.oled_height)

		#scanning method
		self.L2R_U2D = 1
		self.L2R_D2U = 2
		self.R2L_U2D = 3
		self.R2L_D2U = 4
		self.U2D_L2R = 5
		self.U2D_R2L = 6
		self.D2U_L2R = 7
		self.D2U_R2L = 8
		self.OLED_Scan_Dir = scanMethod

		self.grayH = 0xf0
		self.grayL = 0x0f
		
		self.OLED_Normal_Display = 0xa4
		self.OLED_Inverse_Display = 0xa7
		self.OLED_Activate_Scroll = 0x2f
		self.OLED_Desactivate_Scroll = 0x2e
		self.OLED_Set_Brightness = 0x81

		self.OLED_Scroll_Left = 0x26
		self.OLED_Scroll_Right = 0x27

		self.OLED_Scroll_2Frames = 0x7
		self.OLED_Scroll_3Frames = 0x4
		self.OLED_Scroll_4Frames = 0x5
		self.OLED_Scroll_5Frames = 0x0
		self.OLED_Scroll_25Frames = 0x6
		self.OLED_Scroll_64Frames = 0x1
		self.OLED_Scroll_128Frames = 0x2
		self.OLED_Scroll_256Frames = 0x3
		
		self.NULLBYTE = 0x00
		self.FFBYTE = 0xff

	def GPIO_Init(self):
		GPIO.setwarnings(False)
		GPIO.setup(self.OLED_RST_PIN, GPIO.OUT)
		GPIO.setup(self.OLED_DC_PIN, GPIO.OUT)
		GPIO.setup(self.OLED_CS_PIN, GPIO.OUT)
		return 0
		
	def SPI4W_Write_Byte(self,data):
		self.SPI.writebytes(data)
		
	def I2C_Write_Byte(self, data, Cmd):
		self.bus.write_byte_data(self.OLED_Address, Cmd, data)

	def Driver_Delay_ms(self, xms):
		time.sleep(xms / 1000.0)
			
	"""    Hardware reset     """
	def OLED_Reset(self):
		GPIO.output(self.OLED_RST_PIN, GPIO.HIGH)
		self.Driver_Delay_ms(100)
		GPIO.output(self.OLED_RST_PIN, GPIO.LOW)
		self.Driver_Delay_ms(100)
		GPIO.output(self.OLED_RST_PIN, GPIO.HIGH)
		self.Driver_Delay_ms(100)

	"""    Write register address and data     """
	def writeCommand(self, cmd=None):
		if self.USE_SPI_4W == 1:
			GPIO.output(self.OLED_DC_PIN, GPIO.LOW)
			GPIO.output(self.OLED_CS_PIN, GPIO.LOW)
			if type(cmd).__name__ == "int": cmd = [cmd]
			self.SPI.writebytes(cmd)
			GPIO.output(self.OLED_CS_PIN, GPIO.HIGH)
		else :
			if type(cmd).__name__ == "list":
				for byte in cmd:
					self.bus.write_byte_data(self.OLED_Address, self.OLED_Command_Mode, byte)
			elif type(cmd).__name__ == "int":
				self.bus.write_byte_data(self.OLED_Address, self.OLED_Command_Mode, cmd)

	def writeData(self, dat=None):
		if self.USE_SPI_4W == 1:
			GPIO.output(self.OLED_DC_PIN, GPIO.HIGH)
			GPIO.output(self.OLED_CS_PIN, GPIO.LOW)
			if type(dat).__name__ == "int": dat = [dat]
			self.SPI.writebytes(dat)
			GPIO.output(self.OLED_CS_PIN, GPIO.HIGH)
		else :
			if type(dat).__name__ == "list" and len(dat) <= 32:
				self.bus.write_i2c_block_data(self.OLED_Address, self.OLED_Data_Mode, dat)
			elif type(dat).__name__ == "int":
				self.bus.write_byte_data(self.OLED_Address, self.OLED_Data_Mode, dat)

	"""    Common register initialization    """
	def OLED_InitReg(self):
		self.writeCommand(0xae)     #--turn off oled panel

		self.writeCommand(0x15)     #  set column address
		self.writeCommand(0x00)     #  start column   0
		self.writeCommand(0x7f)     #  end column   127

		self.writeCommand(0x75)     #   set row address
		self.writeCommand(0x00)     #  start row   0
		self.writeCommand(0x7f)     #  end row   127

		self.writeCommand(0x81)     # set contrast control
		self.writeCommand(0x80) 

		self.writeCommand(0xa0)     # gment remap
		self.writeCommand(0x51)     #51

		self.writeCommand(0xa1)     # start line
		self.writeCommand(0x00) 

		self.writeCommand(0xa2)     # display offset
		self.writeCommand(0x00) 

		self.writeCommand(0xa4)     # rmal display
		self.writeCommand(0xa8)     # set multiplex ratio
		self.writeCommand(0x7f) 

		self.writeCommand(0xb1)     # set phase leghth
		self.writeCommand(0xf1) 

		self.writeCommand(0xb3)     # set dclk
		self.writeCommand(0x00)     #80Hz:0xc1 90Hz:0xe1   100Hz:0x00   110Hz:0x30 120Hz:0x50   130Hz:0x70     01
 
		self.writeCommand(0xab)     #
		self.writeCommand(0x01)     #

		self.writeCommand(0xb6)     # set phase leghth
		self.writeCommand(0x0f) 

		self.writeCommand(0xbe) 
		self.writeCommand(0x0f) 

		self.writeCommand(0xbc) 
		self.writeCommand(0x08) 

		self.writeCommand(0xd5) 
		self.writeCommand(0x62) 

		self.writeCommand(0xfd) 
		self.writeCommand(0x12) 

	def OLED_SetGramScanWay(self):
		#Get the screen scan direction
		#Get GRAM and OLED width and height
		if(self.OLED_Scan_Dir == self.L2R_U2D):
			self.writeCommand(0xa0)    #gment remap
			self.writeCommand(0x51)    #51	
		elif(self.OLED_Scan_Dir == self.L2R_D2U):#Y
			self.writeCommand(0xa0)    #gment remap
			self.writeCommand(0x41)    #51
		elif(self.OLED_Scan_Dir == self.R2L_U2D):
			self.writeCommand(0xa0)    #gment remap
			self.writeCommand(0x52)    #51
		elif(self.OLED_Scan_Dir == self.R2L_D2U):
			self.writeCommand(0xa0)    #gment remap
			self.writeCommand(0x42)    #51
		else:
			print ('Scan_dir set error')
			return -1
		
		#Get GRAM and OLED width and height
		if(self.OLED_Scan_Dir == self.L2R_U2D or self.OLED_Scan_Dir == self.L2R_D2U or self.OLED_Scan_Dir == self.R2L_U2D or self.OLED_Scan_Dir == self.R2L_D2U):
			self.OLED_Dis_Column = self.oled_width
			self.OLED_Dis_Page = self.oled_height
			self.oled_x_Adjust = self.oled_x
			self.oled_y_Adjust = self.oled_y
		else:
			self.OLED_Dis_Column = self.oled_height
			self.OLED_Dis_Page = self.oled_width
			self.oled_x_Adjust = self.oled_y
			self.oled_y_Adjust = self.oled_x

	def init_OLED(self):
		if (self.GPIO_Init() != 0):
			return -1
			
		#Hardware reset
		self.OLED_Reset()
		
		#Set the initialization register
		self.OLED_InitReg()
		
		#Set the display scan and color transfer modes	
		self.OLED_SetGramScanWay()
		self.Driver_Delay_ms(200)
		
		#Turn on the OLED display
		self.writeCommand(0xAF)
		self.Driver_Delay_ms(120)

	def OLED_SetWindows(self, Xstart, Ystart, Xend, Yend):
		Xstart = min([Xstart,self.OLED_Dis_Column,self.OLED_Dis_Page])
		Xend = min([Xend,self.OLED_Dis_Column,self.OLED_Dis_Page])
		Ystart = min([Ystart,self.OLED_Dis_Column,self.OLED_Dis_Page])
		Yend = min([Yend,self.OLED_Dis_Column,self.OLED_Dis_Page])
		
		self.writeCommand(0x15)
		self.writeCommand(Xstart)
		self.writeCommand(Xend - 1)

		self.writeCommand(0x75)
		self.writeCommand(Ystart)
		self.writeCommand(Yend - 1)
	
	def OLED_SetCursor (self, Xpoint, Ypoint):
		Xpoint = min([Xpoint,self.OLED_Dis_Column])
		Ypoint = min([Ypoint,self.OLED_Dis_Page])
		
		self.writeCommand(0x15)
		self.writeCommand(Xpoint)
		self.writeCommand(Xpoint)

		self.writeCommand(0x75)
		self.writeCommand(Ypoint)
		self.writeCommand(Ypoint)

	def clearDisplay(self):		
		if self.USE_SPI_4W == 1:
			black = [0x00]*4096
			self.writeData(black)
			self.writeData(black)
		else:
			black = [0x00]*32
			for i in xrange(0, 8192, 32): # 32 by 32 faster method
				self.writeData(black)

	def setScroll(self, to="right", startRow=0, endRow=15, startCol=0, endCol=15, speed=2):
		# start-end row and col : 0 - 15
		# speed: 2 frames is the fastest, 256 frames is the slowest
		startRow = startRow * 8 # convert to oled page
		endRow = (endRow * 8) + 7
		startCol = startCol * 4
		endCol = (endCol * 4) + 3
		speed = str(speed)
		speedList = {
			"2":self.OLED_Scroll_2Frames, "3":self.OLED_Scroll_3Frames, "4":self.OLED_Scroll_4Frames, 
			"5":self.OLED_Scroll_5Frames, "25":self.OLED_Scroll_25Frames, "64":self.OLED_Scroll_64Frames, 
			"128":self.OLED_Scroll_128Frames, "256":self.OLED_Scroll_256Frames
		}
		if speed in speedList.keys():
			if to == "left": # go to the left
				self.writeCommand([self.OLED_Scroll_Left , self.NULLBYTE, startRow, speedList[speed], endRow, startCol+8, endCol+8, self.NULLBYTE])	
			else:    # go to the right
				self.writeCommand([self.OLED_Scroll_Right, self.NULLBYTE, startRow, speedList[speed], endRow, startCol+8, endCol+8, self.NULLBYTE])
		else:
			print "error speed",speed

	def activeScroll(self):
		self.writeCommand(self.OLED_Activate_Scroll)

	def desactiveScroll(self):
		self.writeCommand(self.OLED_Desactivate_Scroll)

	def setBrightness(self, value=0):
		# values 0 - 15
		self.grayH = (value << 4) & 0xF0
		self.grayL =  value & 0x0F

	def inverseDisplay(self):
		self.writeCommand(self.OLED_Inverse_Display) # set inverse Display

	def normalDisplay(self):
		self.writeCommand(self.OLED_Normal_Display) # set Normal Display (default)

	def pixelsToOledGray(self, pixels=None):
		# ~ oledssd 1327 structure
		data = []
		for i in xrange(0,len(pixels),2):
			# ~ take two half-bytes side by side to make one
			"""
			1111	1111
			16b		16b
			-> 256 bit
			-> two 16 gray scale pixels
			"""
			# convert gray byte to 0-15 values by >>4
			byte1 = pixels[i]   >> 4
			byte2 = pixels[i+1] >> 4

			byte = 0x00
			byte = byte | ((byte1 << 4) & 0xF0)
			byte = byte | (byte2 & 0x0F)

			data.append(byte)

		return data
		
	def putChar(self, char=None, col=0, row=0):
		col = min([self.oled_cols-1,col])
		row = min([self.oled_rows-1,row])
		data = []
		charArray = AsciiPixels().getCharData(char)
		pixels = []
		for line in charArray: # flat array by each line of char
			pixels.extend(line)
		
		pixels = [(x>0)*255 for x in pixels]
		self.OLED_SetWindows(col*4, row*8, (col*4)+4, (row*8)+8)
		byteArr = self.pixelsToOledGray(pixels)
		
		self.writeData(byteArr)
		
	def showTextAt(self, string="", row=0, col=0, align="left", fill=1): # align: left, center, right; fill: fill row
		length = self.oled_cols - col
		if len(string) > length: # over screen
			for line in textwrap.wrap(string, length):
				line = self.alignText(string=line, align=align, col=col, fill=fill)
				self.putText(string=line, col=col, row=row)
				row += 1
				if row == self.oled_rows: row = 0
		else:
			string = self.alignText(string=string, align=align, col=col, fill=fill)
			self.putText(string=string, col=col, row=row)
			
		self.OLED_SetWindows(0, 0, self.OLED_Dis_Column , self.OLED_Dis_Page)
			
	def putText(self, string=None, col=0, row=0):
		for i,char in enumerate(string):
			c = i+col
			self.putChar(char, col=c, row=row)
			
	def alignText(self, string="", align="left", col=0, fill=1):
		length = len(string)
		lineLength = self.oled_width / 8 # each char have 8 pixels length
		if fill: # fill row
			if align == "center": 
				f = "{:^"+str(lineLength - col)+"}"
				return f.format(string)
			elif align == "right":
				f = "{:>"+str(lineLength - col)+"}"
				return f.format(string)
			else: # left
				f = "{:"+str(lineLength - col)+"}"
				return f.format(string)
		else: # change col pos
			if align == "center":
				col = ((lineLength - col - length) / 2) + col
				return string
			elif align == "right":
				col = lineLength - length
				return string
			else:
				return string
				
	def showPageAt(self, col=0, row=0, data=None):
		col = min([self.oled_cols-1,col])
		row = min([self.oled_rows-1,row])
		if max(data) == 1: data = [(x>0)*255 for x in data]# if page is 0-1 code
		byteArr = self.pixelsToOledGray(data)
		self.OLED_SetWindows(col*4, row*8, (col*4)+4, (row*8)+8)
		self.writeData(byteArr)
		self.OLED_SetWindows(0, 0, self.OLED_Dis_Column , self.OLED_Dis_Page)

	def fillScreen(self, data=None, x=0, y=0):
		self.OLED_SetWindows(x, y, self.OLED_Dis_Column , self.OLED_Dis_Page)
		lenData = min([len(data),8192])
		stepSpi = lenData / 2
		if self.USE_SPI_4W == 1:
			for i in xrange(0, lenData, stepSpi): # 4096 max bytes, spi faster method
				self.writeData(data[i:i+stepSpi])
		else:
			for i in xrange(0, lenData, 32): # 32 max bytes, i2c faster method
				self.writeData(data[i:i+32])
