# Unicorn hat hd class  

**VERSION = "0.0.2"**  

**DATE = "2021 September 15"**  

## Class to use unicorn hat hd  

### Example  

```python
from unihat import UniHat  
u = UniHat()  
u.clear()  
u.scrollText(text="Hello World", xy=(0,4), color=(255,255,0), duration=70) 
data = u.opendData(fileName='pathToFile.dat') # text file with data array [(y,x,r,v,b), (y,x,r,v,b) ... (y,x,r,v,b)] and .dat extension
u.putData(data)  
u.show(duration=2)  # 2 second
data = u.opendImage(file="pathToFile.jpg")
u.putData(data)  
u.show(duration=2)  
u.scrollScreen(direction="left", duration=100)  
u.off()  
```

## Unicornhathd installation   

*code source*  
[on github https://github.com/pimoroni/unicorn-hat-hd](https://github.com/pimoroni/unicorn-hat-hd)  

### Install library for Python 3:  
```bash
sudo apt-get install python3-pip python3-dev python3-spidev  
sudo pip3 install unicornhathd  
```

### Install library for Python 2:  
```bash
sudo apt-get install python-pip python-dev python-spidev  
sudo pip install unicornhathd  
```

### Script auto
```bash  
curl https://get.pimoroni.com/unicornhathd | bash  
```

**Andres Lozano Gallego a.k.a Loz, 2019-2020**  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License http://www.artlibre.org
