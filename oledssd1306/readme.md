# Oled I2C SSD1306

## Python oled class

The class **oledSSD1306.py** allow you to show texte and graphics on a  
[AZDelivery 5 x I2C Display 128 x 64 Pixel 0.96 Pouce](https://www.waveshare.com/0.96inch-oled-b.htm)
Oled display with a Raspberry Pi  

There is an example of code within the class itself  
There is also a script **convert-image-to-oled.py** that convert a picture to an oled binary file   
All this code was tested on a raspberry pi A, B, Zero  

convert-image-to-oled.py require python PIL and numpy library  

**Andres Lozano Gallego a.k.a Loz, 2021.**  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License http://www.artlibre.org
