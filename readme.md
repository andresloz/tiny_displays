# Tiny Displays

## Python code (tested on Raspberry Pi mainly)

### Less is more, no more librairies than python-smbus for i2c and SPI devices 

to drive text, image and animated on  
* Lcd display (serial)  
* Lcd-rgb display (i2c)  
* Oled SSD1327 display 96x96 (i2c)  
* Oled SSD1327 display 128x128 (i2c & SPI)  
* Oled SSD1306 display 128x64 128x32 (i2c & SPI)  
* Oled SSD1308 display 128x64 (i2c)  
* Unicorn HAT HD (spi, 256 RGB LED pixels in a 16x16 matrix)  
* 256 RGB NeoPixels LED in a 16x16 matrix with FadeCandy dithering USB-controlled driver  

**Andres Lozano Gallego a.k.a Loz, 2022.**  
Copyleft: this work is free, you can copy, distribute and modify it  
under the terms of the Free Art License http://www.artlibre.org
