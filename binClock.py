#!/usr/bin/env python
from lcdrgb import RgbLcd
from datetime import datetime
import smbus
import time
import RPi.GPIO as GPIO
from rtc import DS1307

NAME = "Rgb Lcd Binclock"
VERSION = "0.0.2"
DATE = "2021 October 28"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://andre-lozano.org"
DESCRIPTION = "Bin Clock Device with seeedstudio lcd display https://www.seeedstudio.com/Grove-self.LCD_-RGB-Backlight-p-1643.html "

# ~ lcd rgb have 2 lines and 16 chars

# bin clock code
if __name__=="__main__":
	def clockMovement(mv=None,char="system"):
		if mv:
			if char == "system":
				c = "*"
			else:
				c = "-"
				
			if mv == c+" ":
				top = " "+c
				bottom = c+" "
			else:
				top = c+" "
				bottom = " "+c

			return top,bottom
		else:
			return "  ","  "

	def colorTime(val=0,progressive=1):
		if progressive:
			"""
			Color	R		G		B
			0h = 	0,		0,		255
			6h = 	127,	255,	0
			12h = 	255,	255,	0
			18h = 	127,	255,	0
			23h = 	0,		0,		255

			127 / 12 = 21
			255 / 6 = 42
			"""
			maxRgb = 128
			coefMini = maxRgb/12
			coefMaxi = maxRgb/6
			if val < 6:
				t = val
				r = val * coefMini
				g = val * coefMaxi
				b = maxRgb - (val * coefMaxi)
			elif val < 12:
				t = val
				r = val * coefMini
				g = maxRgb
				b = 0
			elif val < 18:
				t = val
				val = val - 12
				r = maxRgb - (val * coefMini)
				g = maxRgb
				b = 0
			elif val < 24:
				t = val
				val = val - 12
				r = maxRgb - (val * coefMini)
				g = maxRgb - ((val-6) * coefMaxi)
				b = (val-6) * coefMaxi
		else:
			"""
			18-6 night = indigo (75,0,130)
			6-10 morning and 14-18 afternoon = olive (128,128,0)
			10-14 midday = orange (255,165,0)
			"""
			if val > 18 or val < 6:
				r,g,b = 75,0,130
			elif 10 > val > 6 or 18 > val > 14:
				r,g,b = 128,128,0
			elif 14 > val > 10:
				r,g,b = 255,165,0
			
		return (r,g,b)

	GPIO.setmode(GPIO.BCM)
	exitButton = 13
	modeButton = 19
	if exitButton: GPIO.setup(exitButton, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	if modeButton: GPIO.setup(modeButton, GPIO.IN, pull_up_down=GPIO.PUD_UP)

	bus = smbus.SMBus(1) # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
	lcd = RgbLcd(bus)

	dt = datetime.now()
	systemTime = str(dt.year)+str(dt.month).zfill(2)+str(dt.day).zfill(2)+str(dt.hour).zfill(2)+str(dt.minute).zfill(2)
	systemTime = int(systemTime)
	
	try:
		dtRtc = DS1307()
		dtRtc.getTime() # try to get time from rtc
		rtcTime = str(2000+dtRtc.year)+str(dtRtc.month).zfill(2)+str(dtRtc.dayOfMonth).zfill(2)+str(dtRtc.hour).zfill(2)+str(dtRtc.minute).zfill(2)
		rtcTime = int(rtcTime)
		if systemTime > rtcTime: # if system time ahead update rtc
			dtRtc.fillByYMD(dt.year,dt.month,dt.day)
			dtRtc.fillByHMS(dt.hour,dt.minute,dt.second)
			dtRtc.setTime()
		elif 5 > rtcTime - systemTime > 0: # if system time is later less than 5 min then update rtc
			dtRtc.fillByYMD(dt.year,dt.month,dt.day)
			dtRtc.fillByHMS(dt.hour,dt.minute,dt.second)
			dtRtc.setTime()
			dtRtc.getTime() # get time from rtc
			rtcTime = str(2000+dtRtc.year)+str(dtRtc.month).zfill(2)+str(dtRtc.dayOfMonth).zfill(2)+str(dtRtc.hour).zfill(2)+str(dtRtc.minute).zfill(2)
			rtcTime = int(rtcTime)
		
	except:
		rtcTime = 0 # if not rtc time 0

	if systemTime >= rtcTime:
		timeSource = "system"
		topMv = "* "
	else:
		timeSource = "rtc"
		topMv = "- "

	delay = 1
	mode = "timeanddate"
	hourColor = 0
	changeMode = 0

	try:
		lcd.clear()
		lcd.backlight()
		# ~ test time colors
		for i in xrange(24):
			color = colorTime(val=i)
			lcd.clear()
			lcd.setRGB(color)
			lcd.setCursor(row=0, col=0)
			lcd.showText("H: "+str(i).zfill(2))
			lcd.setCursor(row=1, col=0)
			lcd.showText(repr(color))
			time.sleep(0.1)
		lcd.clear()

		while True:
			time.sleep(delay)

			exit_state = GPIO.input(exitButton)
			if exit_state == 0:
				lcd.clear()
				lcd.noBacklight()
				exit()

			mode_state = GPIO.input(modeButton)
			if mode_state == 0:
				lcd.clear()
				changeMode = 1
				if mode == "timeanddate":
					mode = "timebin"
				elif mode == "timebin":
					mode = "datebin"
				elif mode == "datebin":
					mode = "timehex"
				elif mode == "timehex":
					mode = "time"
				elif mode == "time":
					mode = "date"
				elif mode == "date":
					mode = "credits"
				elif mode == "credits":
					mode = "timeanddate"
				
				time.sleep(1)

			if timeSource == "system":
				dt = datetime.now()
				hour = str(dt.hour).zfill(2)
				binhour = str(bin(dt.hour))[2:].zfill(8)
				hexhour = hex(dt.hour)[2:].upper().zfill(2)

				minutes = str(dt.minute).zfill(2)
				binminutes = str(bin(dt.minute))[2:].zfill(8)
				hexminutes = hex(dt.minute)[2:].upper().zfill(2)
				
				seconds = str(dt.second).zfill(2)
				binseconds = str(bin(dt.second))[2:].zfill(8)
				hexseconds = hex(dt.second)[2:].upper().zfill(2)

				day = str(dt.day).zfill(2)
				binday = str(bin(dt.day))[2:].zfill(6)
				hexday = hex(dt.day)[2:].upper().zfill(2)

				month = str(dt.month).zfill(2)
				binmonth = str(bin(dt.month))[2:].zfill(4)
				hexmonth = hex(dt.month)[2:].upper().zfill(2)

				year = str(dt.year)
				binyear = str(bin(dt.year))[2:].zfill(12)
				hexyear = hex(dt.year)[2:].upper().zfill(3)
			else:
				dtRtc.getTime()
				hour = str(dtRtc.hour).zfill(2)
				binhour = str(bin(dtRtc.hour))[2:].zfill(8)
				hexhour = hex(dtRtc.hour)[2:].upper().zfill(2)

				minutes = str(dtRtc.minute).zfill(2)
				binminutes = str(bin(dtRtc.minute))[2:].zfill(8)
				hexminutes = hex(dtRtc.minute)[2:].upper().zfill(2)
				
				seconds = str(dtRtc.second).zfill(2)
				binseconds = str(bin(dtRtc.second))[2:].zfill(8)
				hexseconds = hex(dtRtc.second)[2:].upper().zfill(2)

				day = str(dtRtc.dayOfMonth).zfill(2)
				binday = str(bin(dtRtc.dayOfMonth))[2:].zfill(6)
				hexday = hex(dtRtc.dayOfMonth)[2:].upper().zfill(2)

				month = str(dtRtc.dayOfMonth).zfill(2)
				binmonth = str(bin(dtRtc.dayOfMonth))[2:].zfill(4)
				hexmonth = hex(dtRtc.dayOfMonth)[2:].upper().zfill(2)

				year = str(2000+dtRtc.year)
				binyear = str(bin(dtRtc.year))[2:].zfill(12)
				hexyear = hex(dtRtc.year)[2:].upper().zfill(3)

			if int(hour) != hourColor or hourColor == 0 or changeMode == 1: # check every hour or at init or when mode change
				hourColor = int(hour) # store current hour
				changeMode = 0 # reset to 0 changeMode status
				
				color = colorTime(val=dt.hour,progressive=1)
				lcd.setRGB(color)

			topMv, bottomMv = clockMovement(mv=topMv, char=timeSource)
			if mode == "timeanddate":
				lcd.setCursor(row=0, col=0)
				lcd.showText(hour+":"+minutes+":"+seconds+"     "+topMv)
				lcd.setCursor(row=1, col=0)
				lcd.showText(text=day+"/"+month+"/"+year+"   "+bottomMv)
			elif mode == "timebin":
				lcd.setCursor(row=0, col=0)
				lcd.showText("H: "+topMv+"  "+binhour[0:4]+" "+binhour[4:])
				lcd.setCursor(row=1, col=0)
				lcd.showText("M: "+bottomMv+"  "+binminutes[0:4]+" "+binminutes[4:])
			elif mode == "datebin":
				lcd.setCursor(row=0, col=0)
				lcd.showText("D:"+binday[0:2]+" "+binday[2:]+" M:"+binmonth)
				lcd.setCursor(row=1, col=0)
				lcd.showText("Y:"+binyear[0:4]+" "+binyear[4:8]+" "+binyear[8:12])
			elif mode == "timehex":
				lcd.setCursor(row=0, col=0)
				lcd.showText("H:"+hexhour+" M:"+hexminutes+"    "+topMv)
				lcd.setCursor(row=1, col=0)
				lcd.showText("D:"+hexday+" M:"+hexmonth+"  Y:"+hexyear)
			elif mode == "time":
				lcd.setCursor(row=0, col=0)
				lcd.showText("HOUR     "+topMv+"   "+hour)
				lcd.setCursor(row=1, col=0)
				lcd.showText("MINUTES  "+bottomMv+"   "+minutes)
			if mode == "date":
				lcd.setCursor(row=0, col=0)
				lcd.showText(text="DAY "+day+"  MONTH "+month)
				lcd.setCursor(row=1, col=0)
				lcd.showText(text="YEAR        "+year)
			elif mode == "credits":
				lcd.setCursor(row=0, col=0)
				lcd.showText(text="BIN CLOCK BY LOZ")
				lcd.setCursor(row=1, col=0)
				lcd.showText(text="COPYLEFT    2021")

	except KeyboardInterrupt:
		lcd.clear()
		lcd.noBacklight()
		print "close device"

