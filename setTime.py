#!/usr/bin/python
import time
from datetime import datetime
from rtc import DS1307

if __name__=="__main__":
	weekDays = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
	val = raw_input("set time from dialog type 'd': ")
	if val == "d":
		date = raw_input("day/month/year=> 23/8/2021 : ")
		day,month,year = date.split("/")
		weekday = raw_input("week day monday-sunday=> 0-6 : ")

		_time = raw_input("hour/minute/second=> 15/30/0 : ")
		hour,minute,second = _time.split("/")

		day,month,year,weekday,hour,minute,second = map(int,[day,month,year,weekday,hour,minute,second])
	else:
		dtime = datetime.now() 
		year = dtime.year
		month = dtime.month
		day = dtime.day
		weekday = dtime.weekday()
		hour = dtime.hour
		minute = dtime.minute
		second = dtime.second
		for i in xrange(10):
			print i,"sec..."
			time.sleep(1)	# set time one second later to avoid sync
	
	# ~ set time
	print
	print "sync time to rtc..."
	clock = DS1307()
	clock.fillByYMD(year,month,day)
	clock.fillByHMS(hour,minute,second)
	clock.fillDayOfWeek(weekday)
	clock.setTime()
	print
	print "..."
	print
	print "TIME & DATE!"
	dtime = datetime.now()
	print "SYS Time", dtime.hour, ":", dtime.minute, ":", dtime.second, "& Date", weekDays[int(dtime.weekday())], dtime.day, "-", dtime.month, "-", dtime.year 
	clock.getTime()
	print "---rtc is one second before---"
	print "RTC Time", clock.hour, ":", clock.minute, ":", clock.second, "& Date", weekDays[clock.dayOfWeek], clock.dayOfMonth, "-", clock.month, "-", 2000+clock.year
	
