#! /usr/bin/env python
#-*- coding: utf-8 -*-

import smbus, sys, time, tarfile, RPi.GPIO as GPIO
from os import listdir, path, system

from oledssd1327 import OledSSD1327

try:
	import pygame
	audioK = 1
except:
	audioK = 0

NAME = "Show Oled Movie"
VERSION = "0.1.0"
DATE = "2021 October 28"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://hyperficiel.com"
DESCRIPTION = "show text, read data from archive of pictures files and send them to seeedstudio oled display 96xs96 SSD1327"

"""
example with raspberry pi
maybe you need to install pygame and smbus
set i2c baudrate to 480000 for better sync (rapsberry pi)
this script is dedicated for videos done with "Movie Pattern" module from "PMT (pattern maker tools)" by Loz from http://andre-lozano.org
put directories which contains tar archive, sound and frameset in "videos" directory
put videos and tiny_displays directories in the same directory as this file
add "python /home/pi/playOled1306.py &" in /etc/rc.local if you want autostart
"""

def getDirectories(dir=".", exclude=[]):
	directories = []
	for file in sorted(listdir(dir)):
		if path.isdir(path.join(dir, file)):
			if not file in exclude:
				directories.append(path.join(dir, file))

	return directories

def loadMusic(file=None, vol=0.1):
	if audioK:
		try:
			pygame.mixer.music.load(file)
			pygame.mixer.music.set_volume(vol)
		except:
			pass

def playMusic(file=None):
	if audioK:
		try:
			pygame.mixer.music.play()
		except:
			pass

def stopMusic():
	if audioK:
		try:
			pygame.mixer.music.stop()
		except:
			pass


def processFiles(frames=[],videoArchive="",audioArchive="",frameRateFile=""):
	if path.isfile(videoArchive) and path.isfile(audioArchive) and path.isfile(frameRateFile):
		# prepare data
		if len(frames) == 0:# check if frames are yet loaded
			oled.clearDisplay()
			oled.showTextAt(row=startRow, col=0, string="loading...")
			oled.showTextAt(row=startRow+1, col=0, string=directory.split("/")[-1]) # show directory name

			# prepare data
			oled.showTextAt(row=startRow+5, col=0, string="music...")
			loadMusic(audioArchive)
			oled.showTextAt(row=startRow+5, col=0, string="archiv...")
			tar = tarfile.open(videoArchive)
			for i, member in enumerate(tar.getmembers()):
				if i % 25 == 0:
					oled.showTextAt(row=startRow+5, col=0, string="frame: "+str(i))

				f = tar.extractfile(member)
				frames.append(map(ord, f.read()))

				if nextDirButton:
					input_state = GPIO.input(nextDirButton)
					if input_state == 0: # get next directory
						oled.clearDisplay()
						tar.close()
						time.sleep(1)
						return []

				if exitButton:
					exit_state = GPIO.input(exitButton)
					if exit_state == 0:
						oled.clearDisplay()
						tar.close()
						exit()

			tar.close()

		with open(frameRateFile) as f: # 0.04 = 1 sec / 25 frames
			frameDuration = 1 / float(f.read())

		# start process
		elapsedTime = time.time() # start elapsed time
		playMusic(audioArchive)

		for frame in frames:
			oled.fillScreen(data=frame)
			elapsedTime += frameDuration

			timeLag = elapsedTime - time.time()
			if timeLag > 0.01: # control time frame in millisecond
				time.sleep(timeLag)

			if nextDirButton:
				input_state = GPIO.input(nextDirButton)
				if input_state == 0: # get next directory
					stopMusic()
					time.sleep(1)
					return []

			if exitButton:
				exit_state = GPIO.input(exitButton)
				if exit_state == 0: # stop show
					oled.clearDisplay()
					stopMusic()
					exit()

		stopMusic()

	return frames

if __name__ == "__main__":
	GPIO.setmode(GPIO.BCM)

	# button is pin -> button -> ground
	nextDirButton = 26
	exitButton = 19

	if nextDirButton:GPIO.setup(nextDirButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # next directory button
	if exitButton:GPIO.setup(exitButton, GPIO.IN, pull_up_down=GPIO.PUD_UP) # exit from show button

	bus = smbus.SMBus(1) # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
	oled = OledSSD1327(bus)
	oled.init_OLED()

	oled.clearDisplay()

	# example bitmap
	# this is an easy way to create pixels description of one 8x8 pix oled page
	diamond = [ # diamond shape
		0, 0, 0, 1, 1, 0, 0, 0,
		0, 0, 1, 1, 1, 1, 0, 0,
		0, 1, 1, 1, 1, 1, 1, 0,
		1, 1, 1, 1, 1, 1, 1, 1,
		0, 1, 1, 1, 1, 1, 1, 0,
		0, 0, 1, 1, 1, 1, 0, 0,
		0, 0, 0, 1, 1, 0, 0, 0,
		0, 0, 0, 0, 0, 0, 0, 0
	]

	# example text (startup screen)
	startRow = 3
	endCol = 12
	oled.showTextAt(row=startRow+2, string="Micro-Movies", align="center")
	oled.showTextAt(row=startRow+3, string="Andre-Lozano", align="center")

	for i in xrange(endCol):
		oled.showPageAt(row=startRow, col=i, data=diamond)

	for i in xrange(endCol):
		oled.showPageAt(row=startRow+5, col=i, data=diamond)

	time.sleep(3)

	#  movies parameters
	d = 0 # dirs in use (videos)
	frames = [] # frames

	if audioK:
		try:
			pygame.init()
		except:
			audioK = 0

	videoDirectories = getDirectories(dir=path.join(path.dirname(path.realpath(__file__)), "videos"))

	try:
		while True:
			if len(videoDirectories) == 0:exit()
			if len(videoDirectories) == 1: # one directory then loop contigous
				directory = videoDirectories[0]
				videoArchive = path.join(directory, "archive.tar")
				audioArchive = path.join(directory, "audio.mp3")
				frameRateFile = path.join(directory, "framerate.txt")
				oled.clearDisplay()

				frames = processFiles(frames=frames,videoArchive=videoArchive,audioArchive=audioArchive,frameRateFile=frameRateFile)

			elif len(videoDirectories) > 1:
				if d == len(videoDirectories): # loop
					d = 0

				directory = videoDirectories[d]
				videoArchive = path.join(directory, "archive.tar")
				audioArchive = path.join(directory, "audio.mp3")
				frameRateFile = path.join(directory, "framerate.txt")

				processFiles(frames=[],videoArchive=videoArchive,audioArchive=audioArchive,frameRateFile=frameRateFile)
				
				d += 1 # go next directory
	except KeyboardInterrupt:
		oled.clearDisplay()
		try:
			stopMusic()
			print " stop while"
		except:
			print " stop while"
