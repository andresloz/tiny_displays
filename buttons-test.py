19#! /usr/bin/env python
#-*- coding: utf-8 -*-

import time
import RPi.GPIO as GPIO

if __name__ == "__main__":
	GPIO.setmode(GPIO.BCM)
	"""
	|	|	|	|	|	21
	5	6	13	19	26	gnd
	"""

	# button is pin -> button -> ground, resistor: pull up
	button21 = 21
	button26 = 26
	button19 = 19
	button13 = 13
	button6 = 6
	button5 = 5

	if button21:GPIO.setup(button21, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	if button26:GPIO.setup(button26, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	if button19:GPIO.setup(button19, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	if button13:GPIO.setup(button13, GPIO.IN, pull_up_down=GPIO.PUD_UP)
	if button6: GPIO.setup(button6 , GPIO.IN, pull_up_down=GPIO.PUD_UP)
	if button5: GPIO.setup(button5 , GPIO.IN, pull_up_down=GPIO.PUD_UP)


	# Now wait!
	while 1:
		if button21:
			state = GPIO.input(button21)
			if state == 0:
				print button21
				time.sleep(1)

		if button26:
			state = GPIO.input(button26)
			if state == 0:
				print button26
				time.sleep(1)

		if button19:
			state = GPIO.input(button19)
			if state == 0:
				print button19
				time.sleep(1)

		if button13:
			state = GPIO.input(button13)
			if state == 0:
				print button13
				time.sleep(1)

		if button6:
			state = GPIO.input(button6)
			if state == 0:
				print button6
				time.sleep(1)

		if button5:
			state = GPIO.input(button5)
			if state == 0:
				print button5
				time.sleep(1)



